#ifndef STATE_VIEWER_FORM_H
#define STATE_VIEWER_FORM_H

#include <QMainWindow>
#include <QTimer>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>

#include "ros_connect_widget.h"
#include "ros_subscriber_widget.h"
#include "motor_state_display.h"
#include "ros_publisher_widget.h"
#include "data_collector.h"
#include "motor_detail_display.h"
#include "motor_control_widget.h"

// ROS
#include <ros/ros.h>

#include "ui_state_viewer_form.h"

struct t_MotorStateDisplayInitData
{
  std::string sName;
  int center_x;
  int center_y;
  int text_x;
  int text_y;
  bool bLeftToRigth;
};

namespace Ui {
class StateViewerForm;
}

class StateViewerForm : public QMainWindow
{
  Q_OBJECT
public:
  explicit StateViewerForm(int argc, char *argv[],  QWidget *parent = 0);
  void bootstrap();
  ~StateViewerForm();
public slots:
  void logAddString(QString s);

private:
  QStringList slLog;
  QStringList slJointList;
  Ui::StateViewerForm ui;
  QGraphicsScene qGScene;

  QTimer * timer;
  QGraphicsPixmapItem *pixmap_item;
private slots:
  void timer_clk();
};


#endif // STATE_VIEWER_FORM_H
