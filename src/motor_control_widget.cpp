#include "motor_control_widget.h"
#include "ui_motor_control_widget.h"

MotorControlWidget::MotorControlWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::MotorControlWidget)
{
  ui->setupUi(this);
}

MotorControlWidget::~MotorControlWidget()
{
  delete ui;
}

void MotorControlWidget::on_pbAllSel_toggled(bool checked)
{
  std::string s="";
  emit ServoSelect(&s,checked) ;
  ui->pbHeadSel->setChecked(checked);
  ui->pbLeg1Sel->setChecked(checked);
  ui->pbLeg2Sel->setChecked(checked);
  ui->pbLeg3Sel->setChecked(checked);
  ui->pbLeg4Sel->setChecked(checked);
}

void MotorControlWidget::on_pbLeg1Sel_toggled(bool checked)
{
  std::string s="leg1";
  emit ServoSelect(&s,checked) ;
}

void MotorControlWidget::on_pbLeg2Sel_toggled(bool checked)
{
  std::string s="leg2";
  emit ServoSelect(&s,checked) ;
}

void MotorControlWidget::on_pbLeg3Sel_toggled(bool checked)
{
  std::string s="leg3";
  emit ServoSelect(&s,checked) ;
}

void MotorControlWidget::on_pbLeg4Sel_toggled(bool checked)
{
  std::string s="leg4";
  emit ServoSelect(&s,checked) ;
}

void MotorControlWidget::on_pbHeadSel_toggled(bool checked)
{
  std::string s="head";
  emit ServoSelect(&s,checked) ;
}

void MotorControlWidget::on_pbClearError_clicked()
{
    emit SendCommand(COMMAND_CLEAR_ERROR);
}

void MotorControlWidget::on_pbReset_clicked()
{
    emit SendCommand(COMMAND_RESET);
}

void MotorControlWidget::on_pbTorqueOn_clicked()
{
    emit SendCommand(COMMAND_TORQUE_ON);
}

void MotorControlWidget::on_pbTorqueOff_clicked()
{
    emit SendCommand(COMMAND_TORQUE_OFF);
}
