#ifndef DATA_COLLECTOR_H
#define DATA_COLLECTOR_H

#include <QString>
#include <QObject>
#include <QList>


#include <sweetie_bot_herkulex_msgs/HerkulexJointState.h>
#include <sweetie_bot_herkulex_msgs/HerkulexState.h>

#define   ERROR_OVER_VOLTAGE      0x01
#define   ERROR_POT_LIMIT         0x02
#define   ERROR_TEMPERATURE       0x04
#define   ERROR_INVALID_PACKET    0x08
#define   ERROR_OVERLOAD          0x10
#define   ERROR_DRIVER_FAULT      0x20
#define   ERROR_EEP_REGS          0x40
#define   ERROR_MASK              0x77

#define   DETAIL_MOVING                         0x01
#define   DETAIL_INPOSITION                     0x02
#define   DETAIL_INVALID_PACKET_CHECKSUM        0x04
#define   DETAIL_INVALID_PACKET_UNKNOWN_CMD     0x08
#define   DETAIL_INVALID_PACKET_REG_RANGE       0x10
#define   DETAIL_INVALID_PACKET_FRAME_ERROR     0x20
#define   DETAIL_MOTOR_ON                       0x40

#define   TORQUE_MODE_BREAK_ON     0x40
#define   TORQUE_MODE_TORQUE_ON    0x60
#define   TORQUE_MODE_TORQUE_FREE  0x00

#define   LED_MODE_GREEN        0x01
#define   LED_MODE_BLUE         0x02
#define   LED_MODE_RED          0x04
#define   TEMP_GREEN    60
#define   TEMP_YELLOW   70
#define   TEMP_RED      75
#define   TEMP_DARKRED  80

#define   MAX_OFFLINE_CNT 1000

struct MotorData
{
  std::string name;
  std::string noresponse;
  int iHJS_offline_counter=0;
  int iHS_offline_counter=0;
  bool HJS_respond_sucess=false;
  double HJS_pos=0;
  double HJS_vel=0;
  double HJS_pwm=0;
  double HJS_pos_goal=0;
  double HJS_pos_desired=0;
  double HJS_vel_desired=0;
  uint8_t HJS_status_error=0;
  uint8_t HJS_status_detail=0;

  bool HS_respond_sucess=false;
  uint8_t HS_torque_control=0;
  uint8_t HS_led_control=0;
  double HS_voltage=0;
  double HS_temperature=0;
  uint8_t HS_status_error=0;
  uint8_t HS_status_detail=0;
};
bool compare(double value1, double value2, quint8 precision);

class DataCollector : public QObject
{
  Q_OBJECT
public:
  explicit DataCollector(QObject *parent = nullptr);
  void AddNewMotorName(std::string sName);

private:

  QList<MotorData> qlData;

 signals:
  void Notify(const MotorData *pData);
  void logString(QString s);

public slots:
  void HJS_NewMsg(sweetie_bot_herkulex_msgs::HerkulexJointState msg);
  void HS_NewMsg(sweetie_bot_herkulex_msgs::HerkulexState msg);
};

#endif // DATA_COLLECTOR_H
