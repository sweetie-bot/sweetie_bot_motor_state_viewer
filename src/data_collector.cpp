#include "data_collector.h"

DataCollector::DataCollector(QObject *parent) : QObject(parent)
{

}

void DataCollector::AddNewMotorName(std::string sName)
{
 foreach (MotorData element,qlData)  // ищем имя движка в массиве данных
  {
    if (element.name==sName) return;
  }
 MotorData a;
 a.name=sName;
 qlData.push_back(a);
 //emit logString("Add new Motor  "+QString::fromStdString(sName));
}

void DataCollector::HJS_NewMsg(sweetie_bot_herkulex_msgs::HerkulexJointState msg)
{
  unsigned long size_of_msg=msg.name.size();
  QString s;
  MotorData mdElement;
 // s=QString::number(size_of_msg);
 // emit logString("новое сообщение "+s);
  for(unsigned long i=0; i<size_of_msg; i++)  // перебираем все сообщения
  {
   // foreach (MotorData element,qlData)  // ищем имя движка в массиве данных
    for (int j=0;j<qlData.size();j++)
    {
      if (qlData.at(j).name==msg.name.at(i)) // нашли, заполняем значения полей структуры
      {
        mdElement=qlData.at(j);
        mdElement.HJS_respond_sucess=true;
        if (msg.not_responding.size()>0) mdElement.noresponse=msg.not_responding.at(i);
        if (msg.pos.size()>0) mdElement.HJS_pos=msg.pos.at(i);
        if (msg.vel.size()>0) mdElement.HJS_vel=msg.vel.at(i);
        if (msg.pwm.size()>0) mdElement.HJS_pwm=msg.pwm.at(i);
        if (msg.pos_goal.size()>0) mdElement.HJS_pos_goal=msg.pos_goal.at(i);
        if (msg.pos_desired.size()>0) mdElement.HJS_pos_desired=msg.pos_desired.at(i);
        if (msg.vel_desired.size()>0) mdElement.HJS_vel_desired=msg.vel_desired.at(i);
        if (msg.status_error.size()>0) mdElement.HJS_status_error=msg.status_error.at(i);
        if (msg.status_detail.size()>0) mdElement.HJS_status_detail=msg.status_detail.at(i);
        qlData[j]=mdElement;
        emit Notify(&qlData.at(j));  // отсылаем сигнал
        s.fromStdString(qlData.at(j).noresponse);
        if (s.size()!=0) emit logString("нет ответа от "+s);
        /*****************************/
        // вставить обработку сторки с неответившими движками
        /*****************************/
      }
    } // foreach
  } // for
}

void DataCollector::HS_NewMsg(sweetie_bot_herkulex_msgs::HerkulexState msg)
{
  MotorData mdElement;
    //foreach (MotorData element,qlData)  // ищем имя движка в массиве данных
  for (int j=0;j<qlData.size();j++)
    {
      if (qlData.at(j).name==msg.name) // нашли, заполняем значения полей структуры
      {
        mdElement=qlData.at(j);
        if (msg.respond_sucess==false)
        {
          mdElement.iHS_offline_counter++;
          break; // пакет неверный
        }
        mdElement.iHS_offline_counter=0;
        mdElement.HS_respond_sucess=msg.respond_sucess;
        mdElement.HS_torque_control=msg.torque_control;
        mdElement.HS_led_control=msg.led_control;
        mdElement.HS_voltage=msg.voltage;
        mdElement.HS_temperature=msg.temperature;
        mdElement.HS_status_error=msg.status_error;
        mdElement.HS_status_detail=msg.status_detail;
        qlData[j]=mdElement;
        emit Notify(&qlData.at(j));  // отсылаем сигнал
      }
    } // foreach
}

bool compare(double value1, double value2, quint8 precision)
{
    return std::abs(value1 - value2) < std::pow(10, -precision);
}

