#ifndef MOTOR_STATE_DISPLAY_H
#define MOTOR_STATE_DISPLAY_H

//#include <QWidget>
#include <QTimer>
#include <QTextStream>
//#include <QGraphicsItemGroup>
#include <QGraphicsObject>
//#include <QGraphicsItem>
#include <QGraphicsView>
#include <QBitmap>
#include <QColor>
#include <QtMultimedia/QMediaPlayer>
#include "data_collector.h"

#include <sweetie_bot_herkulex_msgs/HerkulexJointState.h>

class MotorStateDisplay : public QGraphicsObject
{
  Q_OBJECT
public:
  explicit MotorStateDisplay(std::string Name, int x, int y,bool bLeftToRigth);
  virtual ~MotorStateDisplay();

private:
  void updateStatus();
  void updateTextParam();
  void sendLogMsg();
  void updateCircleColor();
  void updateLineOut();
  void setMainCircleColor(Qt::GlobalColor cPen,Qt::GlobalColor cBrish);
  bool bOnlineFlag=false;
  bool bMouseHover=false;
  bool bShowTextToggle=false;
  bool bIsSelected=false;
  void showTextParam(bool bVisible);
  void updateTextParamDisplay();
  void showLine(bool bVisible);
  QRectF boundingRect() const  ;
  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
  virtual void	hoverEnterEvent(QGraphicsSceneHoverEvent * event);
  virtual void	hoverLeaveEvent(QGraphicsSceneHoverEvent * event);
  virtual void	mousePressEvent(QGraphicsSceneMouseEvent * event);
  void changeColor(QPixmap *pixmap,QColor old_color,QColor new_color);

  int lineout_x,lineout_y;
  bool bIsLeftToRigthFlag;

  QGraphicsEllipseItem *main_circle,*contour;
  QGraphicsSimpleTextItem *gRespondStatusText,*gNameText,*gTempText,*gErrorText,*gDetailText;
  QPixmap pmDangerIcon, pmHiTempIcon, pmOverloadIcon, pmVoltageIcon, pmErrorIcon;
  QGraphicsLineItem *pLine1,*pLine2;
  QVector <QGraphicsPixmapItem*> vErrIcon;

  unsigned int iOfflineCounter=5;
  std::string sName;
  MotorData mdMotorData;
  bool bDataChangeFlag=false;

  QMediaPlayer * player;

  QTimer * timer;

signals:
  void logString(QString s);
  void servoClick (std::string sServoName);
  void PublishCommand(std::string sName, uint8_t uiCommand);

private slots:
  void SendCommand(uint8_t uiCommand);
  void timer_clk();
  void MotorDataUpdate(const MotorData *pData);
  void Select(std::string *name, bool bIsSelect);
};

#endif // MOTOR_STATE_DISPLAY_H
