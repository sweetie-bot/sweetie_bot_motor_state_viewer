#include "ros_subscriber_widget.h"
#include "ui_ros_subscriber_widget.h"

ros_subscriber_widget::ros_subscriber_widget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::ros_subscriber_widget)
{
  ui->setupUi(this);
}

ros_subscriber_widget::~ros_subscriber_widget()
{
  delete ui;
}

void ros_subscriber_widget ::RosStateChanged(int iState)
{
  iRosSate=iState;
  if (iRosSate==0)
  {
    ui->lbState->setText("online");
    rx_node=new ros::NodeHandle ;
    hjs_sub = rx_node->subscribe<sweetie_bot_herkulex_msgs::HerkulexJointState>("/motion/herkulex/joint_states", 10000, &ros_subscriber_widget::HjsCallback, this);
    hs_sub = rx_node->subscribe<sweetie_bot_herkulex_msgs::HerkulexState>("/motion/herkulex/servo_states", 10000, &ros_subscriber_widget::HsCallback, this);
   // sc_sub = rx_node->subscribe<sweetie_bot_herkulex_msgs::ServoCommands>("/motion/herkulex/servo_comands", 10000, &ros_subscriber_widget::ScCallback, this);
  }
    else
  {
        ui->lbState->setText("offline ");// +QString::number(iState));
       hjs_sub.shutdown();
       hs_sub.shutdown();
      // sc_sub.shutdown();
        rx_node->shutdown();
        rx_node=nullptr;
   }
}

void ros_subscriber_widget::HjsCallback(const sweetie_bot_herkulex_msgs::HerkulexJointState::ConstPtr& msg)
{
  unsigned long iMsgSize=0;
  hjs_msgs=*msg;
  i_HJS_Count++;
  iMsgSize=hjs_msgs.name.size();
  ui->lbHjsMsgCount->setText(QString::number(i_HJS_Count));
  emit HJS_GetNewMsg(hjs_msgs);
}
/*
void ros_subscriber_widget::ScCallback(const sweetie_bot_herkulex_msgs::ServoCommands::ConstPtr& msgs)
{
  sc_msgs=*msgs;
  std::string sName;
  uint8_t uiCommand;
  sName=sc_msgs.name.at(0);
  uiCommand=sc_msgs.command;
  QString s;
  s="Servo "+QString::fromStdString(sName)+" Get command " +QString::number(uiCommand);
  emit  logString(s);

  i_SC_Count++;
  ui->lbScMsgCount->setText(QString::number(i_SC_Count));
}
*/
void ros_subscriber_widget::HsCallback(const sweetie_bot_herkulex_msgs::HerkulexState::ConstPtr& msgs)
{
  unsigned long iMsgSize=0;
  hs_msgs=*msgs;
  i_HS_Count++;
  iMsgSize=hs_msgs.name.size();
  ui->lbHsMsgCount->setText(QString::number(i_HS_Count));
  emit HS_GetNewMsg(hs_msgs);
}
