#include "motor_state_display.h"

#define   I_ELIPS_SIZE      34
#define   I_ELIPS_W         5
#define   I_CONTOUR_W       4
#define   I_CONTOUR_SIZE    (I_ELIPS_SIZE+I_CONTOUR_W*2)
#define   I_LINE_SIZE       20
#define   I_ICON_SIZE       32
#define   I_TEXT_BLOCK_W    90
#define   I_TEXT_BLOCK_H    80
#define   I_TEXT_STRING_H   20

MotorStateDisplay::MotorStateDisplay(std::string Name,int x, int y,bool bLeftToRigth)
  :QGraphicsObject()
{
  sName=Name;
  lineout_x=x;
  lineout_y=y;
  bIsLeftToRigthFlag=bLeftToRigth;
  timer = new QTimer(this);
  connect(timer, SIGNAL(timeout()), this, SLOT(timer_clk()));
  timer->start(100);
  this->setAcceptedMouseButtons(Qt::AllButtons );
  this->setAcceptHoverEvents(true);

  QPen pen(Qt::black);
  pen.setWidthF(I_ELIPS_W);
  main_circle=new QGraphicsEllipseItem(-I_ELIPS_SIZE/2,-I_ELIPS_SIZE/2,I_ELIPS_SIZE,I_ELIPS_SIZE);
  main_circle->setPen(pen);
  main_circle->setBrush(Qt::black);
  main_circle->setParentItem(this);
  main_circle->setZValue(9999); // на передний план
  contour =new QGraphicsEllipseItem(-I_CONTOUR_SIZE/2,-I_CONTOUR_SIZE/2,I_CONTOUR_SIZE,I_CONTOUR_SIZE);
  pen.setWidthF(I_CONTOUR_W);
  pen.setColor(Qt::white);
  contour->setPen(pen);
  contour->setBrush(Qt::transparent);
  contour->setParentItem(this);
  contour->setZValue(999);

  /*********   текстовая информация под выноской*******/
  int iTextX,iTextY;
  if (bIsLeftToRigthFlag)iTextX=lineout_x-I_TEXT_BLOCK_W;
  else iTextX=lineout_x;
  iTextY=lineout_y;

  gRespondStatusText=new QGraphicsSimpleTextItem(this);
  gNameText=new QGraphicsSimpleTextItem(this);
  gTempText=new QGraphicsSimpleTextItem(this);
  gErrorText=new QGraphicsSimpleTextItem(this);
  gDetailText=new QGraphicsSimpleTextItem(this);
  gRespondStatusText->setBrush(Qt::blue);
  gNameText->setBrush(Qt::blue);
  gTempText->setBrush(Qt::blue);
  gErrorText->setBrush(Qt::blue);
  gDetailText->setBrush(Qt::blue);
  gNameText->setPos(iTextX,iTextY+I_TEXT_STRING_H*0);
  gRespondStatusText->setPos(iTextX,iTextY+I_TEXT_STRING_H*1);
  gTempText->setPos(iTextX,iTextY+I_TEXT_STRING_H*2);
  gErrorText->setPos(iTextX,iTextY+I_TEXT_STRING_H*3);
  gDetailText->setPos(iTextX,iTextY+I_TEXT_STRING_H*4);
  updateTextParamDisplay();

  /**************   линии выноски ****************/
  pLine1=new  QGraphicsLineItem(0,0,lineout_x,lineout_y,this) ;
  pLine2=new  QGraphicsLineItem(this) ;
  pLine1->setZValue(1); // на задний план
  showLine(false);
  /************ пиктограммы **************************/
  pmDangerIcon.load(":resource/images/icon_danger_30.png");
  changeColor(&pmDangerIcon,QColor(0,0,0), QColor(255,0,0));
  pmHiTempIcon.load(":resource/images/icon_hi_temp_30.png");
  pmOverloadIcon.load(":resource/images/icon_overload_30.png");
  pmVoltageIcon.load(":resource/images/icon_voltage_30.png");
  pmErrorIcon.load(":resource/images/icon_error_30.png");
  /**************************************************************/

  player = new QMediaPlayer;
  // ...
  player->setMedia(QUrl("qrc:/resource/sounds/ouch.mp3"));

}

MotorStateDisplay::~MotorStateDisplay()
{
  delete player;
}


QRectF MotorStateDisplay::boundingRect() const
{
  /*
  // если не показывается текст, то граничный прямоугольник определяется только размером элипса
  if (!bShowTextToggle)return QRectF(0,0,I_ELIPS_SIZE,I_ELIPS_SIZE);
  // а если отображается текст, то все зависит от направления
  int x1,y1,w,h;
  w=I_TEXT_BLOCK_W+I_LINE_SIZE+I_ELIPS_SIZE; // ширина не меняется
  if (bIsLeftFlag) x1=0-I_LINE_SIZE-I_TEXT_BLOCK_W;
  else  x1=0;
  if (bIsUpFlag)y1=0-I_LINE_SIZE;
  else y1=0;
  if (bIsUpFlag) h=I_TEXT_BLOCK_H;
  else   h=I_ELIPS_SIZE+I_LINE_SIZE+I_TEXT_BLOCK_H;
  return QRectF(x1,y1,w,h);
  */
  return QRectF(-I_ELIPS_SIZE/2,-I_ELIPS_SIZE/2,I_ELIPS_SIZE,I_ELIPS_SIZE);
}

void MotorStateDisplay::timer_clk()
{
  /*********  если данные перестали поступать - ставим флаг bOnlineFlag **/
  if (iOfflineCounter>MAX_OFFLINE_CNT)
  {
    if (bOnlineFlag==true)
    {
      bOnlineFlag=false;
      bDataChangeFlag=true;
    }
  }
  else
  {
    iOfflineCounter++;
    if (bOnlineFlag==false)
    {
      bOnlineFlag=true;
      bDataChangeFlag=true;
    }
  }
  /**************************************************************/
  /*** запускаем перерисовку, если данные изменились ***/
  if (bDataChangeFlag)
  {
    updateStatus(); // обновляем пиктограммы
    updateTextParam();// обновляем параметры
    updateCircleColor(); // изменение цвета круга
    //sendLogMsg(); // сообщение в лог
    bDataChangeFlag=false;
  }

}
/********************* MotorDataUpdate ********/
/*   вызывается как слот из pDataCollector
 *  проверяет изменились ли отслеживаемые параметры
 *  ставит флаг об изменениях, сохраняет новые данные
 * ***************************************************/
void MotorStateDisplay::MotorDataUpdate(const MotorData *pData)
{
  if ((pData->name)!=sName)return;
  iOfflineCounter=0;
  if (!compare(pData->HS_voltage,mdMotorData.HS_voltage,1))bDataChangeFlag=true;
  if ((pData->HS_led_control)!=mdMotorData.HS_led_control) bDataChangeFlag=true;
  if (!compare(pData->HS_temperature,mdMotorData.HS_temperature,2))bDataChangeFlag=true;
  if ((pData->HS_status_error)!=mdMotorData.HS_status_error) bDataChangeFlag=true;
  if ((pData->HJS_status_error)!=mdMotorData.HJS_status_error) bDataChangeFlag=true;
  if ((pData->HS_status_detail)!=mdMotorData.HS_status_detail) bDataChangeFlag=true;
  if ((pData->HJS_status_detail)!=mdMotorData.HJS_status_detail) bDataChangeFlag=true;
  if ((pData->HS_torque_control)!=mdMotorData.HS_torque_control) bDataChangeFlag=true;
  if ((pData->HS_respond_sucess)!=mdMotorData.HS_respond_sucess) bDataChangeFlag=true;
  if ((pData->HJS_respond_sucess)!=mdMotorData.HJS_respond_sucess) bDataChangeFlag=true;
  mdMotorData=*pData;
}

void MotorStateDisplay::updateTextParam()
{
  if (mdMotorData.HS_respond_sucess)gRespondStatusText->setText("online");
  else gRespondStatusText->setText("no_respond");
  QString s;
  gNameText->setText(QString::fromStdString(sName));
  s=QString::number(mdMotorData.HS_temperature,'f',1);
  gTempText->setText("Temp= "+s);
  s=QString::number(mdMotorData.HJS_status_error,16);
  gErrorText->setText("Error= "+s);
  s=QString::number(mdMotorData.HJS_status_detail,16);
  gDetailText->setText("Detail= "+s);
}

void MotorStateDisplay::sendLogMsg()
{
  QString s;
  if (bOnlineFlag)s="online";
  else s="offline";
  emit logString(QString::fromStdString(sName)+
                 " New status:"+s+
                 " detail= "+QString::number(mdMotorData.HJS_status_detail,16)+
                 " error=" +QString::number(mdMotorData.HJS_status_error,16) );
}

void MotorStateDisplay::updateCircleColor()
{
  if (!bOnlineFlag) // если нет сообщений - делаем серый цвет
  {
    setMainCircleColor(Qt::darkGray,Qt::lightGray);
    return;
  }
  Qt::GlobalColor cBorderColor,cCenterCollor;
  cBorderColor=Qt::darkGray;
  /****  цвет окантовки зависит от температуры ******/
  if (mdMotorData.HS_temperature<=TEMP_GREEN)cBorderColor=Qt::blue;
  if (mdMotorData.HS_temperature>TEMP_GREEN)cBorderColor=Qt::green;
  if (mdMotorData.HS_temperature>TEMP_YELLOW)cBorderColor=Qt::yellow;
  if (mdMotorData.HS_temperature>TEMP_RED) cBorderColor=Qt::red;

  if (mdMotorData.HS_temperature>TEMP_DARKRED) {
    cBorderColor=Qt::darkRed;
    //player->setVolume(int(round(mdMotorData.HS_temperature)));
    player->play();
  }

  /***  цвет середины показывает работу двигателя *
   *  серый - двигатель оффлайн по данным из HerkulexJointState.msg
   *  желтый - DETAIL_MOTOR_ON по данным из HerkulexJointState.msg
   *  зеленый - двигатель выключен и TORQUE_MODE_BREAK_ON | TORQUE_MODE_TORQUE_ON
   *  белый - двигатель выключен и TORQUE_MODE_TORQUE_FREE
   * ******************************************************/
  if (DETAIL_MOTOR_ON == (mdMotorData.HS_status_detail & DETAIL_MOTOR_ON)){
    cCenterCollor=Qt::darkCyan;
  }
  else
  {
    cCenterCollor=Qt::green;
    if (TORQUE_MODE_TORQUE_FREE == mdMotorData.HS_torque_control)
      cCenterCollor=Qt::white;
  }
  if (!mdMotorData.HS_respond_sucess) cCenterCollor=Qt::lightGray;

  setMainCircleColor(cBorderColor,cCenterCollor)  ;
}

void MotorStateDisplay::updateStatus()
{
  uint8_t bError=mdMotorData.HJS_status_error;
  // отчищаем массив пиктограмм
  qDeleteAll(vErrIcon);
  vErrIcon.clear();
  // заполняем массив пиктограммами
  QGraphicsPixmapItem *pGPI;
  if (bError & ERROR_TEMPERATURE)
  {
    pGPI=new QGraphicsPixmapItem(pmHiTempIcon,this);
    vErrIcon.append(pGPI);
  }
  if (bError & ERROR_OVERLOAD)
  {
    pGPI=new QGraphicsPixmapItem(pmOverloadIcon,this);
    vErrIcon.append(pGPI);
  }
  if (bError & ERROR_OVER_VOLTAGE)
  {
    pGPI=new QGraphicsPixmapItem(pmVoltageIcon,this);
    vErrIcon.append(pGPI);
  }
  if ((bError & ERROR_EEP_REGS)|(bError & ERROR_DRIVER_FAULT))
  {
    pGPI=new QGraphicsPixmapItem(pmErrorIcon,this);
    vErrIcon.append(pGPI);
  }
  if ((bError & ERROR_INVALID_PACKET)|(bError & ERROR_POT_LIMIT))
  {
    pGPI=new QGraphicsPixmapItem(pmDangerIcon,this);
    vErrIcon.append(pGPI);
  }
  /************** рисуем пиктограммы *********************/
  for (int i = 0; i < vErrIcon.size(); i++)
  {
    if (bIsLeftToRigthFlag) vErrIcon.at(i)->setX(lineout_x-I_ICON_SIZE-i*I_ICON_SIZE);
    else vErrIcon.at(i)->setX(lineout_x+i*I_ICON_SIZE);
    vErrIcon.at(i)->setY(lineout_y-I_ICON_SIZE);
  }
  /*******************************************************/

  /************ рисуем линию выноски  **********************/
  int x2;  // зависят от направлекния
  if (bIsLeftToRigthFlag) x2=lineout_x-(vErrIcon.size()*I_ICON_SIZE);
  else x2=lineout_x+(vErrIcon.size()*I_ICON_SIZE);
    pLine2->setLine(lineout_x,lineout_y,x2,lineout_y);
  /*************************************************************/
  updateLineOut();
}

void MotorStateDisplay::updateLineOut()
{
  bool bShowLine=false;
 // if (bShowTextToggle) bShowLine=true;
  if (vErrIcon.size()>0) bShowLine=true;
  if (bMouseHover) bShowLine=true;
  if (bIsSelected) bShowLine=true;
  showLine(bShowLine);
}

void MotorStateDisplay::updateTextParamDisplay()
{
  bool bShowText=false;
 // if (bShowTextToggle) bShowText=true;
  if (bMouseHover) bShowText=true;
  if (bIsSelected) bShowText=true;
  showTextParam(bShowText);
}

void MotorStateDisplay::showLine(bool bVisible)
{
  if (bVisible)
  {
    pLine1->show();
    pLine2->show();
    this->setZValue(9999);
  }
  else
  {
    pLine1->hide();
    pLine2->hide();
    this->setZValue(0);
  }
}

void MotorStateDisplay::showTextParam(bool bVisible)
{
  contour->setVisible(bVisible);
  if (bVisible)
  {
    gRespondStatusText->show();
    gNameText->show();
    gTempText->show();
    gErrorText->show();
    gDetailText->show();
  }
  else
  {
    gRespondStatusText->hide();
    gNameText->hide();
    gTempText->hide();
    gErrorText->hide();
    gDetailText->hide();
  }
}
void MotorStateDisplay::setMainCircleColor(Qt::GlobalColor cPen,Qt::GlobalColor cBrish)
{
  QPen pen(cPen);
  pen.setWidthF(I_ELIPS_W);
  main_circle->setPen(pen);
  main_circle->setBrush(cBrish);
}

void MotorStateDisplay::changeColor(QPixmap *pixmap,QColor old_color,QColor new_color)
{

  QImage img=pixmap->toImage();
  for (int i = 0; i < img.width(); i++)
  {
      for (int j = 0; j < img.height(); j++)
      {
          if (img.pixelColor(i,j)==old_color) img.setPixelColor(i,j,new_color);
      }
  }
 *pixmap = QPixmap::fromImage(img);
}

void MotorStateDisplay::Select(std::string *name, bool bIsSelect)
{
  std::string::size_type n;
  n=sName.find(*name);
  if (n!=std::string::npos) bIsSelected=bIsSelect;
  updateLineOut();
  updateTextParamDisplay();
}

void MotorStateDisplay::SendCommand(uint8_t uiCommand)
{
  if (bIsSelected) emit PublishCommand(sName,uiCommand);
}

void MotorStateDisplay::hoverEnterEvent(QGraphicsSceneHoverEvent * event)
{
  bMouseHover=true;
  updateTextParamDisplay();
  updateLineOut();
  QGraphicsObject::hoverEnterEvent(event);
}

void MotorStateDisplay::hoverLeaveEvent(QGraphicsSceneHoverEvent * event)
{
  bMouseHover=false;
  updateTextParamDisplay();
  updateLineOut();
  QGraphicsObject::hoverLeaveEvent(event);
}

void MotorStateDisplay::mousePressEvent(QGraphicsSceneMouseEvent * event)
{
  bIsSelected=!bIsSelected;
  bMouseHover=bIsSelected;
  updateTextParamDisplay();
  updateLineOut();
  emit servoClick(sName);
  QGraphicsObject::mousePressEvent(event);
}

void MotorStateDisplay::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  Q_UNUSED(painter);  Q_UNUSED(option);  Q_UNUSED(widget);
}
