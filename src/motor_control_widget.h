#ifndef MOTOR_CONTROL_WIDGET_H
#define MOTOR_CONTROL_WIDGET_H

#define COMMAND_CLEAR_ERROR   1
#define COMMAND_RESET         2
#define COMMAND_TORQUE_ON     4
#define COMMAND_TORQUE_OFF    8

#include <QWidget>

namespace Ui {
class MotorControlWidget;
}

class MotorControlWidget : public QWidget
{
  Q_OBJECT

public:
  explicit MotorControlWidget(QWidget *parent = nullptr);
  ~MotorControlWidget();
signals:
  void ServoSelect(std::string *sName, bool bSelect);
  void SendCommand(uint8_t uiCommand);
private slots:
  void on_pbTorqueOff_clicked();
  void on_pbTorqueOn_clicked();
  void on_pbReset_clicked();
  void on_pbClearError_clicked();
  void on_pbHeadSel_toggled(bool checked);
  void on_pbLeg4Sel_toggled(bool checked);
  void on_pbLeg3Sel_toggled(bool checked);
  void on_pbLeg2Sel_toggled(bool checked);
  void on_pbLeg1Sel_toggled(bool checked);
  void on_pbAllSel_toggled(bool checked);

private:
  Ui::MotorControlWidget *ui;
};

#endif // MOTOR_CONTROL_WIDGET_H
