#ifndef ROS_SUBSCRIBER_WIDGET_H
#define ROS_SUBSCRIBER_WIDGET_H

#include <QWidget>
#include <QLabel>

// ROS
#include <ros/ros.h>
#include <sweetie_bot_herkulex_msgs/HerkulexJointState.h>
#include <sweetie_bot_herkulex_msgs/HerkulexState.h>
#include <sweetie_bot_herkulex_msgs/ServoCommands.h>

namespace Ui {
class ros_subscriber_widget;

}

class ros_subscriber_widget : public QWidget
{
  Q_OBJECT
public:
  explicit ros_subscriber_widget(QWidget *parent = nullptr);
  ~ros_subscriber_widget();
public slots:
  void RosStateChanged(int iState);
signals:
  void HJS_GetNewMsg(sweetie_bot_herkulex_msgs::HerkulexJointState msg);
  void HS_GetNewMsg(sweetie_bot_herkulex_msgs::HerkulexState msg);
  void logString(QString s);
private:
  int iRosSate=0;
  int i_HJS_Count=0;
  int i_HS_Count=0;
  //int i_SC_Count=0;
  ros::NodeHandle *rx_node;
  ros::Subscriber hjs_sub, hs_sub;
//  ros::Subscriber sc_sub;
  sweetie_bot_herkulex_msgs::HerkulexJointState hjs_msgs;
  sweetie_bot_herkulex_msgs::HerkulexState hs_msgs;
//  sweetie_bot_herkulex_msgs::ServoCommands sc_msgs;

  void HjsCallback(const sweetie_bot_herkulex_msgs::HerkulexJointState::ConstPtr& msgs);
  void HsCallback(const sweetie_bot_herkulex_msgs::HerkulexState::ConstPtr& msgs);
 // void ScCallback(const sweetie_bot_herkulex_msgs::ServoCommands::ConstPtr& msgs);
  Ui::ros_subscriber_widget *ui;
};

#endif // ROS_SUBSCRIBER_WIDGET_H
