#ifndef MOTOR_DETAIL_DISPLAY_H
#define MOTOR_DETAIL_DISPLAY_H

#include <QWidget>
#include <QTimer>
#include "data_collector.h"

namespace Ui {
class MotorDetailDisplay;
}

class MotorDetailDisplay : public QWidget
{
  Q_OBJECT

public:
  explicit MotorDetailDisplay(QWidget *parent = nullptr);
  ~MotorDetailDisplay();
private slots:
  void timer_clk();
  void servoSelected(std::string sServoName);
  void MotorDataUpdate(const MotorData *pData);
private:
  void updateText();
  int iOfflineCounter=0;
  bool bOnlineFlag;
  MotorData mdMotorData;
  std::string sName;
  QTimer * timer;
  Ui::MotorDetailDisplay *ui;
};

#endif // MOTOR_DETAIL_DISPLAY_H
