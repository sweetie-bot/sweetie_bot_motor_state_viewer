#include "motor_detail_display.h"
#include "ui_motor_detail_display.h"

MotorDetailDisplay::MotorDetailDisplay(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::MotorDetailDisplay)
{
  ui->setupUi(this);
  timer = new QTimer(this);
  connect(timer, SIGNAL(timeout()), this, SLOT(timer_clk()));
  timer->start(100);
  iOfflineCounter=MAX_OFFLINE_CNT+1;
  bOnlineFlag=false;
}

MotorDetailDisplay::~MotorDetailDisplay()
{
  delete ui;
}
void MotorDetailDisplay::servoSelected(std::string sServoName)
{
  sName=sServoName;
  ui->lbServoName->setText(QString::fromStdString(sServoName));
  bOnlineFlag=true;
}

void MotorDetailDisplay::timer_clk()
{
  /*********  если данные перестали поступать - помечаем серву как оффлайн **/
  if (iOfflineCounter>MAX_OFFLINE_CNT)
  {
    if (bOnlineFlag==true)
    {
      bOnlineFlag=false;
      ui->lbServoName->setText(QString::fromStdString(sName)+ " offline");
    }
  }
  else
  {
    iOfflineCounter++;
    if (bOnlineFlag==false)
    {
      bOnlineFlag=true;
      ui->lbServoName->setText(QString::fromStdString(sName)+" online");
    }
  }
}

void MotorDetailDisplay::MotorDataUpdate(const MotorData *pData)
{
  if ((pData->name)!=sName)return;
  iOfflineCounter=0;
  mdMotorData=*pData;
  updateText();
}
void MotorDetailDisplay::updateText()
{
  ui->lbTemp->setText(QString::number(mdMotorData.HS_temperature,'f',1));
  ui->lbVoltage->setText(QString::number(mdMotorData.HS_voltage,'f',2));
  switch (mdMotorData.HS_led_control)
  {
  case LED_MODE_RED:
    ui->lbLedControl->setText("Red");
    break;
  case LED_MODE_BLUE:
    ui->lbLedControl->setText("Blue");
    break;
  case LED_MODE_GREEN:
    ui->lbLedControl->setText("Green");
    break;
  default:
    ui->lbLedControl->setText("Unknow");
  }
  switch (mdMotorData.HS_torque_control)
  {
  case TORQUE_MODE_BREAK_ON:
    ui->lbTorqueControl->setText("Break");
    break;
  case TORQUE_MODE_TORQUE_ON:
    ui->lbTorqueControl->setText("On");
    break;
  case TORQUE_MODE_TORQUE_FREE:
    ui->lbTorqueControl->setText("Free");
    break;
  default:
    ui->lbTorqueControl->setText("Unknow");
  }
  if (mdMotorData.HJS_respond_sucess)ui->lbHjsRespond->setText("Online");
  else ui->lbHjsRespond->setText("Offline");
  if (mdMotorData.HS_respond_sucess) ui->lbHsRespond->setText("Online");
  //else ui->lbHsRespond->setText("Offline");
  else ui->lbHsRespond->setText("Offline "+QString::number(mdMotorData.iHS_offline_counter));
  ui->lbPosGoal->setText(QString::number(mdMotorData.HJS_pos_goal));
  ui->lbPosDesired->setText(QString::number(mdMotorData.HJS_pos_desired));
  ui->lbVelDesired->setText(QString::number(mdMotorData.HJS_vel_desired));
  ui->lbPos->setText(QString::number(mdMotorData.HJS_pos));
  ui->lbVel->setText(QString::number(mdMotorData.HJS_vel));
  ui->lbPWM->setText(QString::number(mdMotorData.HJS_pwm));
  uint8_t bError=0,bDetail=0;
  if (ui->rbJointStates->isChecked()) bError=mdMotorData.HJS_status_error;
  if (ui->rbJointStates->isChecked()) bDetail=mdMotorData.HJS_status_detail;
  if (ui->rbServoStates->isChecked()) bError=mdMotorData.HS_status_error;
  if (ui->rbServoStates->isChecked()) bDetail=mdMotorData.HS_status_detail;
  if (bError & ERROR_OVER_VOLTAGE) ui->lbErrOverVoltage->setText("Err");
    else ui->lbErrOverVoltage->setText("OK");
  if (bError & ERROR_POT_LIMIT) ui->lbErrPotLimit->setText("Err");
    else ui->lbErrPotLimit->setText("OK");
  if (bError & ERROR_TEMPERATURE) ui->lbErrTemp->setText("Err");
    else ui->lbErrTemp->setText("OK");
  if (bError & ERROR_INVALID_PACKET) ui->lbErrInvalidPacket->setText("Err");
    else ui->lbErrInvalidPacket->setText("OK");
  if (bError & ERROR_OVERLOAD) ui->lbErrOverload->setText("Err");
    else ui->lbErrOverload->setText("OK");
  if (bError & ERROR_DRIVER_FAULT) ui->lbErrDriverFault->setText("Err");
    else ui->lbErrDriverFault->setText("OK");
  if (bError & ERROR_EEP_REGS) ui->lbErrEepRegs->setText("Err");
    else ui->lbErrEepRegs->setText("OK");
  if (bDetail & DETAIL_MOVING) ui->lbDetailMoving->setText("YES");
    else ui->lbDetailMoving->setText("NO");
  if (bDetail & DETAIL_INPOSITION) ui->lbDetailInpos->setText("YES");
    else ui->lbDetailInpos->setText("NO");
  if (bDetail & DETAIL_INVALID_PACKET_CHECKSUM) ui->lbDetailCheksum->setText("YES");
    else ui->lbDetailCheksum->setText("NO");
  if (bDetail & DETAIL_INVALID_PACKET_UNKNOWN_CMD) ui->lbDetailUnknownCMD->setText("YES");
    else ui->lbDetailUnknownCMD->setText("NO");
  if (bDetail & DETAIL_INVALID_PACKET_REG_RANGE) ui->lbDetailRegRange->setText("YES");
    else ui->lbDetailRegRange->setText("NO");
  if (bDetail & DETAIL_INVALID_PACKET_FRAME_ERROR) ui->lbDetailFrameError->setText("YES");
    else ui->lbDetailFrameError->setText("NO");
  if (bDetail & DETAIL_MOTOR_ON) ui->lbDetailMotorOn->setText("YES");
    else ui->lbDetailMotorOn->setText("NO");
  ui->lbNoResponding->setText(QString::fromStdString(mdMotorData.noresponse));
}
