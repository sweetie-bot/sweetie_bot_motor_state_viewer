#include "state_viewer_form.h"

StateViewerForm::StateViewerForm(int argc, char *argv[], QWidget *parent) :
    QMainWindow(parent)
{
	// setup GUI
     ui.setupUi(this);

  ConnectWidget *pCF1=new ConnectWidget(argc, argv,nullptr);
  ros_subscriber_widget *pRSW=new ros_subscriber_widget(this);
  ros_publisher_widget *pRPW=new ros_publisher_widget(this);
  MotorDetailDisplay *pMDD=new MotorDetailDisplay(this);
  DataCollector *pDataCollector = new DataCollector(this);
  MotorControlWidget *pMotorControl = new MotorControlWidget(this);

  connect(pCF1, SIGNAL(RosStateChanged(int)), pRSW, SLOT(RosStateChanged(int)));
  connect(pCF1, SIGNAL(RosStateChanged(int)), pRPW, SLOT(RosStateChanged(int)));
  connect(pRSW,SIGNAL(HJS_GetNewMsg(sweetie_bot_herkulex_msgs::HerkulexJointState)),pDataCollector,SLOT(HJS_NewMsg(sweetie_bot_herkulex_msgs::HerkulexJointState)));
  connect(pRSW,SIGNAL(HS_GetNewMsg(sweetie_bot_herkulex_msgs::HerkulexState)),pDataCollector,SLOT(HS_NewMsg(sweetie_bot_herkulex_msgs::HerkulexState)));
  connect(pDataCollector,SIGNAL(logString(QString)),this,SLOT(logAddString(QString)));
  connect(pRSW,SIGNAL(logString(QString)),this,SLOT(logAddString(QString)));
  connect(pDataCollector,SIGNAL(Notify(const MotorData*)),pMDD,SLOT(MotorDataUpdate(const MotorData*)));
//  pRPW->setMaximumHeight(50);
 // pRPW->setMinimumHeight(80);
 // pMotorControl->setMinimumHeight(80);
  pMDD->setMinimumHeight(414);
   pMotorControl->setMinimumHeight(193);
  pMDD->setMinimumWidth(512);
//  pMotorControl->setMinimumWidth(512);

 // ui.verticalLayout_3->addWidget(pRPW);
  ui.verticalLayout_3->addWidget(pMotorControl);
  ui.verticalLayout_3->addWidget(pMDD);
  statusBar()->addWidget(pCF1);
  statusBar()->addWidget(pRSW);
  statusBar()->addWidget(pRPW);

  /*********************************************************************************/
  // заполняем данные о имени и местоположении индикаторов состояния двигателей
  /*********************************************************************************/
  /*
   * 1- имя двигателя
   * 2,3 - координаты центра круга
   * 4.5 - точка вывода текста и сообщений
   */

  std::vector <t_MotorStateDisplayInitData> myMsdInitData;

  myMsdInitData.push_back({"leg2_joint1",390,495,300,550,true});
  myMsdInitData.push_back({"leg2_joint2",320,460,200,500,true});
  myMsdInitData.push_back({"leg2_joint3",230,410,100,450,true});
  myMsdInitData.push_back({"leg2_joint4",170,370,300,200,false});
  myMsdInitData.push_back({"leg2_joint5",120,335,200,150,false});
  myMsdInitData.push_back({"leg2_joint6", 50,295,100,100,false});

  myMsdInitData.push_back({"leg1_joint1",476,495,566,550,false});
  myMsdInitData.push_back({"leg1_joint2",546,460,666,500,false});
  myMsdInitData.push_back({"leg1_joint3",636,410,766,450,false});
  myMsdInitData.push_back({"leg1_joint4",696,370,566,200,true});
  myMsdInitData.push_back({"leg1_joint5",746,335,666,150,true});
  myMsdInitData.push_back({"leg1_joint6",816,295,766,100,true});


  myMsdInitData.push_back({"leg4_joint1",395,675,350,800,false});
  myMsdInitData.push_back({"leg4_joint2",330,690,300,650,true});
  myMsdInitData.push_back({"leg4_joint3",325,780,200,700,true});
  myMsdInitData.push_back({"leg4_joint4",310,880,100,750,true});
  myMsdInitData.push_back({"leg4_joint5",290,960,200,900,true});
  myMsdInitData.push_back({"leg4_joint6",270,1030,100,950,true});

  myMsdInitData.push_back({"leg3_joint1",471,675,516,850,true});
  myMsdInitData.push_back({"leg3_joint2",536,690,566,650,false});
  myMsdInitData.push_back({"leg3_joint3",541,780,666,700,false});
  myMsdInitData.push_back({"leg3_joint4",556,880,766,750,false});
  myMsdInitData.push_back({"leg3_joint5",576,960,666,900,false});
  myMsdInitData.push_back({"leg3_joint6",596,1030,766,950,false});

  myMsdInitData.push_back({"head_joint1",433,460,380,550,false});
  myMsdInitData.push_back({"head_joint2",433,370,480,350,false});
  myMsdInitData.push_back({"head_joint3",410,300,350,350,true});
  myMsdInitData.push_back({"head_joint4",456,250,500,300,false});

  /********************************************************************************************/

  /******** графика и фоновая картинка **********************************************/
  qGScene.setSceneRect(QRectF(0,0,866,1080));

  ui.graphicsView-> setScene(&qGScene);
  ui.graphicsView->setStyleSheet("background-color: transparent");
  // загружаем картинку
  QPixmap myPixMap(":resource/images/pony.png");
  // подгоняем её размер под размер "сцены"
  //QPixmap sPixMap=myPixMap.scaled((int)qGScene.sceneRect().width(),(int)qGScene.sceneRect().height(),Qt::AspectRatioMode::KeepAspectRatio,Qt::TransformationMode::SmoothTransformation);
  // ставим её на сцену и перемещаем посередине
  //QGraphicsPixmapItem *qGPI=qGScene.addPixmap(sPixMap);
   //QGraphicsPixmapItem *qGPI=qGScene.addPixmap(myPixMap);
  qGScene.addPixmap(myPixMap);
  //qGPI->setPos((qGScene.width()- sPixMap.width())/2,(qGScene.height()-sPixMap.height())/2);
  /********** создаем индикаторы состояния двигателей  **********/
  MotorStateDisplay *pMSD;
  int text_x,text_y;
  for (unsigned long i=0;i<myMsdInitData.size();i++)
  {
    text_x=myMsdInitData.at(i).center_x-myMsdInitData.at(i).text_x;
    text_y=myMsdInitData.at(i).center_y-myMsdInitData.at(i).text_y;
    pDataCollector->AddNewMotorName(myMsdInitData.at(i).sName);
    pMSD=new MotorStateDisplay(myMsdInitData.at(i).sName, -text_x, -text_y,myMsdInitData.at(i).bLeftToRigth);
    qGScene.addItem(pMSD);
    pMSD->setPos(myMsdInitData.at(i).center_x,myMsdInitData.at(i).center_y);
    connect(pDataCollector,SIGNAL(Notify(const MotorData*)),pMSD,SLOT(MotorDataUpdate(const MotorData*)));
    connect(pMSD,SIGNAL(logString(QString)),this,SLOT(logAddString(QString)));
    connect (pMSD,SIGNAL(servoClick(std::string)),pMDD,SLOT(servoSelected(std::string)));
    connect(pMotorControl,SIGNAL( ServoSelect(std::string*, bool)),pMSD,SLOT(Select(std::string*, bool)));
    connect(pMotorControl,SIGNAL(SendCommand(uint8_t)),pMSD,SLOT(SendCommand(uint8_t)));
    connect(pMSD,SIGNAL(PublishCommand(std::string, uint8_t)),pRPW,SLOT(PublishCommand(std::string, uint8_t)));
  }
  /************************************************************************/
  /************  таймер используется только в этом объекте *****/
  timer = new QTimer(this);
  connect(timer, SIGNAL(timeout()), this, SLOT(timer_clk()));
  timer->start(1000);
}
void StateViewerForm::logAddString(QString s)
{
  slLog.push_back(s);
  if (slLog.size()>1000) slLog.removeFirst();
  ui.textEdit->clear();
  ui.textEdit->append(slLog.join("\n"));
}
void StateViewerForm::timer_clk()
{
  ui.graphicsView->fitInView(qGScene.sceneRect(),Qt::AspectRatioMode::KeepAspectRatio);
}

StateViewerForm::~StateViewerForm()
{
	delete timer;
}
