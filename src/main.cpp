#include "state_viewer_form.h"
#include <QApplication>
#include "ros_connect_widget.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    StateViewerForm *w = new StateViewerForm(argc, argv);
    w->show();
    return a.exec();

}
